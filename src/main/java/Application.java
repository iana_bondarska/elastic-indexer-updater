import actors.DBActor;
import actors.WorkflowActor;
import akka.actor.Actor;
import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.actor.Props;
import messages.CreateIndicesRequest;
import scala.App;

/**
 * Created by Iana Bondarska on 3/4/14.
 */
public class Application  {
    public static void main(String[] args){
     ActorSystem.create().actorOf(new Props(null, WorkflowActor.class,null)).tell(new CreateIndicesRequest(),null);
    }
}
