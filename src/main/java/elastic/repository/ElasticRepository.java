package elastic.repository;

import config.ConfigManager;
import elastic.entity.ElasticIndex;
import elastic.entity.ElasticNode;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import utils.FilesHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/8/13
 * Time: 13:19
 */
public class ElasticRepository implements Repository {
    private ElasticIndex seriesRootIndex;
    private ElasticIndex seriesSlaveIndex;
    private ElasticIndex unsafeIndex;

    private final TransportClient client;

    public ElasticRepository() {
        Settings settings = getClientsSettings();
        client = new TransportClient(settings);

        for (ElasticNode node : ConfigManager.INSTANCE.getElasticSearch().getElasticNodes()) {
            InetSocketTransportAddress address = new InetSocketTransportAddress(node.getHost(), node.getPort());
            client.addTransportAddress(address);
        }

        List<ElasticIndex> indexes = ConfigManager.INSTANCE.getElasticSearch().getElasticIndicies();
        for (ElasticIndex index : indexes) {
            switch (index.getType()) {
                case seriesRoot:
                    seriesRootIndex = index;
                    break;

                case seriesSlave:
                    seriesSlaveIndex = index;
                    break;

                case unsafe:
                    unsafeIndex = index;
                    break;
            }
        }
    }

    private static Settings getClientsSettings() {
        ImmutableSettings.Builder settingsBuilder = ImmutableSettings.settingsBuilder();
        settingsBuilder.put("cluster.name", ConfigManager.INSTANCE.getElasticSearch().getClusterName());

        return settingsBuilder.build();
    }

    //TODO refactor to something dynamic
    public List<ElasticIndex> getAllIndicies() {
        return Arrays.asList(seriesRootIndex, seriesSlaveIndex, unsafeIndex);
    }

    public ElasticIndex getSeriesRootIndex() {
        return seriesRootIndex;
    }

    public ElasticIndex getSeriesSlaveIndex() {
        return seriesSlaveIndex;
    }

    public ElasticIndex getUnsafeIndex() {
        return unsafeIndex;
    }

    @Override
    public Client getClient() {
        return client;
    }

    @Override
    public Map<String, Object> getIndextimeSettings(ElasticIndex index) {
        Map<String, Object> settings = new HashMap<>();
        settings.put("number_of_shards", index.getShardsCount());
        settings.put("number_of_replicas", 0);
        settings.put("refresh_interval", index.getRefreshInterval());

        return settings;
    }

    @Override
    public Map<String, Object> getQuerytimeSettings(ElasticIndex index) {
        Map<String, Object> settings = new HashMap<>();
        settings.put("number_of_replicas", index.getReplicasCount());
        settings.put("refresh_interval", "1s");

        return settings;
    }

    @Override
    public String getDefaultItemStructure() throws IOException {
        return FilesHelper.getStringFromResource("elastic/itemstructure.json");
    }

    @Override
    public String getDefaultWarmer() throws IOException {
        return FilesHelper.getStringFromResource("elastic/warmer.json");
    }

    @Override
    public String getDefaultAnalysis() throws IOException {
        return FilesHelper.getStringFromResource("elastic/itemanalysis.json");
    }
}
