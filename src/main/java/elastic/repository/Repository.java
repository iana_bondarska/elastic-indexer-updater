package elastic.repository;

import elastic.entity.ElasticIndex;
import org.elasticsearch.client.Client;

import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 11:27
 */
public interface Repository {
    Client getClient();

    Map<String, Object> getIndextimeSettings(ElasticIndex index);

    Map<String, Object> getQuerytimeSettings(ElasticIndex index);

    String getDefaultItemStructure() throws IOException;

    String getDefaultWarmer() throws IOException;

    String getDefaultAnalysis() throws IOException;
}
