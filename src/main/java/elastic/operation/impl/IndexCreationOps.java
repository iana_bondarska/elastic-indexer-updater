package elastic.operation.impl;


import elastic.entity.ElasticIndex;
import elastic.operation.BaseRepositoryOps;
import elastic.repository.Repository;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.settings.loader.SettingsLoaderFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 11:26
 */
public class IndexCreationOps extends BaseRepositoryOps {
    public IndexCreationOps(Repository repository) {
        super(repository);
    }

    public void createDefaultIndexWithMappings(List<ElasticIndex> elasticIndecies) throws Exception {
        for (ElasticIndex elasticIndex : elasticIndecies)
            createDefaultIndexWithMappings(elasticIndex);
    }

    public CreateIndexResponse createDefaultIndexWithMappings(ElasticIndex elasticIndex) throws Exception {
        String index = elasticIndex.getElIndex();
        if(isIndexExists(index))
            throw new IllegalStateException(String.format("Could not run indexing. Index '%s' exists.", index));

        CreateIndexRequestBuilder indexRequestBuilder = getRepository().getClient().admin().indices()
                .prepareCreate(index);
        indexRequestBuilder.addMapping(elasticIndex.getElType(), getRepository().getDefaultItemStructure());
        indexRequestBuilder.setSettings(getDefaultCreationSettings(elasticIndex));

        return indexRequestBuilder.execute().actionGet();
    }

    private Settings getDefaultCreationSettings(ElasticIndex elasticIndex) throws IOException {
        ImmutableSettings.Builder immutableSettings = ImmutableSettings.settingsBuilder();

        Map<String, Object> defaultCreateIndexSettings = getRepository().getIndextimeSettings(elasticIndex);
        for (Map.Entry<String, Object> entry : defaultCreateIndexSettings.entrySet())
            immutableSettings.put(entry.getKey(), entry.getValue());

        String defaultAnalysis = getRepository().getDefaultAnalysis();
        immutableSettings.put(SettingsLoaderFactory.loaderFromSource(defaultAnalysis).load(defaultAnalysis));

        return immutableSettings.build();
    }

    /*private void deleteIndexIfExists(String index) throws InterruptedException {
        if (isIndexExists(index)) {
            getLogger().info(String.format("!!! Index '%s' exists and will be deleted !!!", index));
            getRepository().getClient().admin().indices().prepareDelete(index).execute().actionGet();

            while (isIndexExists(index)) {
                getLogger().info(String.format("Waiting %d secs until all nodes are notified about index drop", 5));
                TimeUnit.SECONDS.sleep(5);
            }
        }
    }*/

    private boolean isIndexExists(String index) {
        return getRepository().getClient().admin().indices().prepareExists(index).execute().actionGet().isExists();
    }
}
