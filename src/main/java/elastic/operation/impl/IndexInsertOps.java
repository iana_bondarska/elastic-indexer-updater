package elastic.operation.impl;


import elastic.entity.ElasticIndex;
import elastic.operation.BaseRepositoryOps;
import elastic.repository.ElasticRepository;
import entity.impl.Item;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 13:38
 */
public class IndexInsertOps extends BaseRepositoryOps {
    private static final Logger logger = LoggerFactory.getLogger("IndexInsertOps");

    public IndexInsertOps(ElasticRepository repository) {
        super(repository);
    }

    public void saveItemsInBulk(List<Item> items) {
        if (items == null || items.isEmpty())
            return;

        BulkRequestBuilder bulkRequest = getRepository().getClient().prepareBulk();
        for (Item item : items) {
            ElasticIndex index;
            if (item.getMods().isUnsafe())
                index = ((ElasticRepository) getRepository()).getUnsafeIndex();
            else
                index = item.getSeries().isSeriesRoot() ? ((ElasticRepository) getRepository()).getSeriesRootIndex()
                                                        : ((ElasticRepository) getRepository()).getSeriesSlaveIndex();

            IndexRequestBuilder indexRequest = getDefaultIndexRequest(index, String.valueOf(item.getId()));
            indexRequest.setSource(item.cleanForIndex());

            bulkRequest.add(indexRequest);
        }
        BulkResponse response = bulkRequest.execute().actionGet();
        if (response.hasFailures())
            logger.error(response.buildFailureMessage());
    }

    private IndexRequestBuilder getDefaultIndexRequest(ElasticIndex elasticIndex, String id) {
        return getIndexRequest(elasticIndex.getElIndex(), elasticIndex.getElType(), id);
    }

    private IndexRequestBuilder getIndexRequest(String index, String type, String id) {
        return getRepository().getClient().prepareIndex(index, type, id);
    }
}
