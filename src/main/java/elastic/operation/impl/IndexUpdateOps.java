package elastic.operation.impl;

import com.google.common.base.Strings;
import config.ConfigManager;
import elastic.entity.ElasticIndex;
import elastic.entity.ElasticNode;
import elastic.operation.BaseRepositoryOps;
import elastic.repository.Repository;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 15:29
 */
public class IndexUpdateOps extends BaseRepositoryOps {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public IndexUpdateOps(Repository repository) {
        super(repository);
    }

    public void updateIndexSettingsToDefaults(List<ElasticIndex> elasticIndecies) {
        for (ElasticIndex elasticIndex : elasticIndecies)
            updateIndexSettingsToDefaults(elasticIndex);
    }

    public void updateIndexSettingsToDefaults(ElasticIndex elasticIndex) {
        String indexName = elasticIndex.getElIndex();
        getRepository().getClient().admin().indices().prepareUpdateSettings(indexName)
                .setSettings(getRepository().getQuerytimeSettings(elasticIndex)).execute().actionGet();
        putWarmer(indexName, "main_warmer");
        executePostIndexingOps(indexName);
        updateAliases(indexName, elasticIndex.getElPreviousIndex(), elasticIndex.getElAlias());
    }

    private void putWarmer(String indexName, String warmerName) {
        //There is a strange bug in elasticsearch JavaAPI. Using RestAPI.
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            ElasticNode connectionNode = ConfigManager.INSTANCE.getElasticSearch().getElasticNodes().get(0);
            HttpPut request = new HttpPut(connectionNode.toString() + "/" + indexName + "/_warmer/" + warmerName);

            String warmer = getRepository().getDefaultWarmer();
            StringEntity input = new StringEntity(warmer);
            request.setEntity(input);

            httpClient.execute(request);
        } catch (IOException e) {
            logger.error("Could not put warmer", e);
        }
    }

    private void updateAliases(String indexName, String oldIndexName, List<String> aliases) {
        if (aliases != null && !aliases.isEmpty()) {
            IndicesAliasesRequestBuilder removeRequest = getRepository().getClient().admin().indices().prepareAliases();
            IndicesAliasesRequestBuilder addRequest = getRepository().getClient().admin().indices().prepareAliases();
            for (String alias : aliases) {
                if (!Strings.isNullOrEmpty(oldIndexName))
                    removeRequest.removeAlias(oldIndexName, alias);
                addRequest.addAlias(indexName, alias);
            }

            try {
                removeRequest.execute().actionGet();
            } catch (Throwable e) {/*The alis does not exist*/}

            addRequest.execute().actionGet();
        }
    }

    private void executePostIndexingOps(String indexName) {
        getLogger().info(String.format("!!! Performing post indexing optimizations for index '%s' !!!", indexName));
        getRepository().getClient().admin().indices().prepareFlush(indexName).setFull(true).execute().actionGet();
        getRepository().getClient().admin().indices().prepareRefresh(indexName).execute().actionGet();
        getRepository().getClient().admin().indices().prepareOptimize(indexName).setWaitForMerge(true).setFlush(true)
                .execute().actionGet();
        getRepository().getClient().admin().indices().prepareClearCache(indexName).execute().actionGet();
    }
}
