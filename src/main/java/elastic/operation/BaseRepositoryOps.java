package elastic.operation;

import elastic.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 11:27
 */
public abstract class BaseRepositoryOps {
    private final Repository repository;
    private final Logger logger;

    public BaseRepositoryOps(Repository repository) {
        this.repository = repository;
        this.logger = LoggerFactory.getLogger(getClass().getSimpleName());
    }

    public Logger getLogger() {
        return logger;
    }

    public Repository getRepository() {
        return repository;
    }
}
