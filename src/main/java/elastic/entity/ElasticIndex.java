package elastic.entity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/17/13
 * Time: 5:03 PM
 */
public class ElasticIndex {
    private String type;
    private String elIndex;
    private String elPreviousIndex;
    private String elType;
    private List<String> elAlias;
    private int shardsCount;
    private int replicasCount;
    private String refreshInterval;

    public Type getType() {
        return Type.valueOf(type);
    }

    public String getElIndex() {
        return elIndex;
    }

    public String getElPreviousIndex() {
        return elPreviousIndex;
    }

    public String getElType() {
        return elType;
    }

    public List<String> getElAlias() {
        return elAlias;
    }

    public int getShardsCount() {
        return shardsCount;
    }

    public int getReplicasCount() {
        return replicasCount;
    }

    public String getRefreshInterval() {
        return refreshInterval;
    }

    public enum Type {
        seriesRoot, seriesSlave, unsafe
    }
}
