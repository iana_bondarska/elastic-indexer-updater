package elastic.entity;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/17/13
 * Time: 5:02 PM
 */
public class ElasticNode {
    private String host;
    private int port;

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "http://" + host + ":" + (port - 100/*brute force 9300 to 9200*/);
    }
}
