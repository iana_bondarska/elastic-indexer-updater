package messages;

import config.ConfigManager;
import entity.impl.Item;
import entity.mapping.FullMapping;

/**
 * Created by Iana Bondarska on 3/3/14.
 */
public class FetchItemsForCreateRequest {
    private final int itemsPerRequest;

    private final String query;
    public FetchItemsForCreateRequest(int itemsPerRequest){
    StringBuilder sqlBuilder = new StringBuilder();
    sqlBuilder.append(FullMapping.selectStatementFromColumns(Item.getIndexMappings()));
    sqlBuilder.append("FROM deposit_items ").append("LEFT JOIN aligator_engine_deposit.manager_user ")
    .append("ON aligator_engine_deposit.manager_user.id = deposit_items.user_id ")
    .append("LEFT JOIN seller ").append("ON seller.user_id = deposit_items.user_id ")
    .append("LEFT JOIN seller_internal_info ")
    .append("ON seller_internal_info.user_id = deposit_items.user_id ")
    .append("LEFT JOIN deposit_item_stats ")
    .append("ON deposit_item_stats.deposit_item_id = deposit_items.deposit_item_id ")
    .append("LEFT JOIN api_promotions ").append("ON api_promotions.ex_data = deposit_items.user_id ")
    .append("LEFT JOIN deposit_item_serie ")
    .append("ON deposit_item_serie.deposit_item_id = deposit_items.deposit_item_id ")
    .append("WHERE deposit_items.status = 'approved' ");

    int limitOnlineTimestampAfter = ConfigManager.INSTANCE.getJdbc().getLimitOnlineTimestampAfter();
    if (limitOnlineTimestampAfter > 0)
            sqlBuilder.append("AND online_timestamp > ").append(limitOnlineTimestampAfter).append(" ");

    if (ConfigManager.INSTANCE.getJdbc().isRootsOnly()) {
        sqlBuilder.append("AND (NOT (deposit_items.is_nudity = 'yes')) ")
                .append("AND (deposit_item_serie.serie_id = 0 OR deposit_item_serie.serie_id IS NULL ")
                .append("OR deposit_item_serie.master) ");
    }
    int limitItemsCount;
    if ((limitItemsCount = ConfigManager.INSTANCE.getJdbc().getLimitItemsCount()) > 0)
            sqlBuilder.append(" LIMIT ").append(limitItemsCount);
    this.query = sqlBuilder.toString();
    this.itemsPerRequest=itemsPerRequest;
    }

    public int getItemsPerRequest() {
        return itemsPerRequest;
    }

    public String getQuery() {
        return query;
    }
}
