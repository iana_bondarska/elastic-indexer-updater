package messages;

import entity.impl.Item;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Iana Bondarska on 3/3/14.
 */
public class FetchItemsResponse {
    private final List<Item> items;
    private final int itemsCount;
    public FetchItemsResponse(int itemsCount) {
        this.itemsCount = itemsCount;
        items = new ArrayList<>(itemsCount);
    }
  public void addItem(Item item){
      items.add(item);
  }
public boolean isComplete(){
    return this.items.size()==itemsCount;
}
    public boolean isEmpty(){
        return this.items.isEmpty();
    }
}
