package messages;

import entity.impl.Item;
import entity.mapping.FullMapping;

/**
 * Created by Iana Bondarska on 3/3/14.
 */
public class FetchItemsForUpdateRequest {
    private String query;
    private final int itemsPerRequest;
    public FetchItemsForUpdateRequest(long timeStamp, int itemsPerRequest){
        this.itemsPerRequest = itemsPerRequest;
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append(FullMapping.selectStatementFromColumns(Item.getIndexMappings()));
        sqlBuilder.append("FROM deposit_items ").append("LEFT JOIN aligator_engine_deposit.manager_user ")
                .append("ON aligator_engine_deposit.manager_user.id = deposit_items.user_id ")
                .append("LEFT JOIN seller ").append("ON seller.user_id = deposit_items.user_id ")
                .append("LEFT JOIN seller_internal_info ")
                .append("ON seller_internal_info.user_id = deposit_items.user_id ")
                .append("LEFT JOIN deposit_item_stats ")
                .append("ON deposit_item_stats.deposit_item_id = deposit_items.deposit_item_id ")
                .append("LEFT JOIN api_promotions ").append("ON api_promotions.ex_data = deposit_items.user_id ")
                .append("LEFT JOIN deposit_item_serie ")
                .append("ON deposit_item_serie.deposit_item_id = deposit_items.deposit_item_id ")
                .append("WHERE deposit_items.status = 'approved' ");
        sqlBuilder.append("AND online_timestamp > ").append(timeStamp).append(" ");
        query = sqlBuilder.toString();
    }
    public String getQuery() {
        return query;
    }

    public int getItemsPerRequest() {
        return itemsPerRequest;
    }
}
