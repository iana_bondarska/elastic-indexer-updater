package utils;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 18:51
 */
public class FilesHelper {
    public static String getStringFromResource(String resourceFullPath) throws IOException {
        try (InputStream inputStream = FilesHelper.class.getClassLoader().getResourceAsStream(resourceFullPath)) {
            return readFromInputStream(inputStream);
        }
    }

    public static String getStringFromFile(File file) throws IOException {
        try (InputStream inputStream = new FileInputStream(file)) {
            return readFromInputStream(inputStream);
        }
    }

    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder result = new StringBuilder();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = bufferedReader.readLine()) != null)
                result.append(line);
        }

        return result.toString();
    }
}
