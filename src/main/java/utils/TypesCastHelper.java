package utils;

import com.google.common.base.Strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/15/13
 * Time: 10:49
 */
public class TypesCastHelper {
    private static final Map<String, Boolean> stringToBooleanMap = new HashMap<>();

    static {
        stringToBooleanMap.put("yes", true);
        stringToBooleanMap.put("no", false);
    }

    public static boolean getBooleanFromYesNo(String value) {
        if (Strings.isNullOrEmpty(value))
            return false;

        return stringToBooleanMap.get(value);
    }

    public static String[] parseArrayString(String rawString){
        if (rawString != null && rawString.length() >= 1024) {
            throw new IndexOutOfBoundsException(
                    "Concatenated string reached 1024 symbols. Change group_concat_max_len.");
        }

        return  Strings.isNullOrEmpty(rawString) ? new String[0] : rawString.split("\\|");
    }
}