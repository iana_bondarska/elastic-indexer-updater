package db;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/20/13
 * Time: 10:48 PM
 */
public interface SqlExecutor<T, E extends Exception> {
    T execute(String sql) throws E;

    T executeCursor(String sql) throws E;
}
