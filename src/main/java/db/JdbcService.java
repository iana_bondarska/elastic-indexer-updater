package db;


import com.mchange.v2.c3p0.DataSources;
import com.mchange.v2.log.MLevel;
import config.ConfigManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/8/13
 * Time: 13:16
 */
public class JdbcService implements SqlExecutor<ResultSetConnection,SQLException> {
    private final DataSource pooledDataSource;
    private final DataSource unPooledDataSource;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final static JdbcService INSTANCE = new JdbcService();

    static {
        com.mchange.v2.log.MLog.getLogger().setLevel(MLevel.INFO);
    }

    public static JdbcService getInstance() {
        return INSTANCE;
    }

    private JdbcService() {
        try {
            unPooledDataSource = getUnPooledDataSource();
            pooledDataSource = DataSources.pooledDataSource(getUnPooledDataSource(), getPooledOptions());
        } catch (Exception e) {
            logger.error("JdbcService initialization", e);
            throw new RuntimeException(e);
        }
    }

    private DataSource getUnPooledDataSource() throws SQLException {
        return DataSources.unpooledDataSource(ConfigManager.INSTANCE.getJdbc().getUrl(),
                                              ConfigManager.INSTANCE.getJdbc().getUsername(),
                                              ConfigManager.INSTANCE.getJdbc().getPassword());
    }

    private Map<String, Object> getPooledOptions(){
        Map<String, Object> pooledOptions = new HashMap<>();
        pooledOptions.put("maxPoolSize", ConfigManager.INSTANCE.getJdbc().getMaxPoolSize());
        pooledOptions.put("minPoolSize", ConfigManager.INSTANCE.getJdbc().getMinPoolSize());
        pooledOptions.put("maxStatements", ConfigManager.INSTANCE.getJdbc().getMaxStatements());
        pooledOptions.put("maxStatementsPerConnection", ConfigManager.INSTANCE.getJdbc().getMaxStatementsPerConnection());
        pooledOptions.put("acquireIncrement", ConfigManager.INSTANCE.getJdbc().getAcquireIncrement());

        return pooledOptions;
    }

    public void shutdownConnectionPool() {
        try {
            DataSources.destroy(pooledDataSource);
            DataSources.destroy(unPooledDataSource);
        } catch (SQLException e) {
            logger.error("JdbcService", e);
        }
    }

    public Connection getConnectionFromPool() throws SQLException {
        return pooledDataSource.getConnection();
    }

    public Connection getDirectConnection() throws SQLException {
        return unPooledDataSource.getConnection();
    }

    @Override
    public ResultSetConnection execute(final String sql) throws SQLException {
        return new ResultSetConnection(getDirectConnection()).execute(sql);
    }

    @Override
    public ResultSetConnection executeCursor(final String sql) throws SQLException {
        return new ResultSetConnection(getDirectConnection()).executeCursor(sql);
    }
}
