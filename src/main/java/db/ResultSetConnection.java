package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/20/13
 * Time: 10:49 PM
 */
public class ResultSetConnection implements AutoCloseable, SqlExecutor<ResultSetConnection, SQLException> {
    private final Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    ResultSetConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ResultSetConnection execute(String sql) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);

        return execute();
    }

    @Override
    public ResultSetConnection executeCursor(String sql) throws SQLException {
        preparedStatement = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        preparedStatement.setFetchSize(Integer.MIN_VALUE);

        return execute();
    }

    @Override
    public void close() throws SQLException {
        if (!resultSet.isClosed())
            resultSet.close();
        if (!preparedStatement.isClosed())
            preparedStatement.close();
        if (!connection.isClosed())
            connection.close();
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    private ResultSetConnection execute() throws SQLException {
        resultSet = preparedStatement.executeQuery();

        return this;
    }
}
