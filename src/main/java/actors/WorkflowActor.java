package actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinRouter;
import config.ConfigManager;
import messages.*;

/**
 * Created by Iana Bondarska on 3/3/14.
 */
public class WorkflowActor extends UntypedActor {
    private final ActorRef aggregatorRouter;
    private final ActorRef dbRouter;
    private final ActorRef indicesRouter;

    public WorkflowActor() {
        aggregatorRouter = this.getContext().actorOf(new Props(null, AggregatorActor.class,null).withRouter(new RoundRobinRouter(10)),
                "aggregatorRouter");
        dbRouter = this.getContext().actorOf(new Props(null, DBActor.class,null).withRouter(new RoundRobinRouter(10)),
                "dbRouter");
        indicesRouter = this.getContext().actorOf(new Props(null, IndicesActor.class,null).withRouter(new RoundRobinRouter(10)),
                "indicesRouter");
    }

    @Override
    public void onReceive(Object o) throws Exception {
     processMessage(o);
    }
  private void processMessage(CreateIndicesRequest createIndicesRequest){
   dbRouter.tell(new FetchItemsForCreateRequest(ConfigManager.INSTANCE.getElasticSearch().getItemsPerBulk()), getSelf());
  }
  private void processMessage(UpdateIndicesRequest updateIndicesRequest){
      dbRouter.tell(new FetchItemsForUpdateRequest(ConfigManager.INSTANCE.getElasticSearch().getItemsPerBulk(), 1000000), getSelf());
  }
    private void processMessage(AggregateItemsResponse aggregateItemsResponse){
   indicesRouter.tell(new GenerateIndicesRequest(),getSelf());
    }
    private void processMessage(FetchItemsResponse fetchItemsResponse){


        aggregatorRouter.tell(new AggregateItemsRequest(),getSelf());
    }


private void processMessage (Object o){
 throw new RuntimeException("Unknown request");
}

}
