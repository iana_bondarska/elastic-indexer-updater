package actors;

import akka.actor.UntypedActor;
import db.JdbcService;
import db.ResultSetConnection;
import entity.impl.Item;
import messages.FetchItemsForCreateRequest;
import messages.FetchItemsForUpdateRequest;
import messages.FetchItemsResponse;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iana Bondarska on 3/3/14.
 */
public class DBActor extends UntypedActor {
    @Override
    public void onReceive(Object o) throws Exception {
     processMessage(o);
    }
private void processMessage(Object o){
    throw new RuntimeException("do not understand message");
}
private void processMessage(FetchItemsForCreateRequest request){
    try{
 ResultSetConnection executedCursor = JdbcService.getInstance().executeCursor(request.getQuery());
    ResultSet resultSet = executedCursor.getResultSet();
    while (resultSet.next()) {
        FetchItemsResponse response=new FetchItemsResponse(request.getItemsPerRequest());
        response.addItem(new Item().fetchFrom(resultSet));
         if (response.isComplete()||resultSet.isAfterLast()) {
        getSender().tell(response,getSelf());
        }
     }
    }
    catch (Exception e){
        throw new RuntimeException(e);
    }

}
private void processMessage(FetchItemsForUpdateRequest request){
    try{
        ResultSetConnection executedCursor = JdbcService.getInstance().executeCursor(request.getQuery());
        ResultSet resultSet = executedCursor.getResultSet();
        while (resultSet.next()) {
            FetchItemsResponse response=new FetchItemsResponse(request.getItemsPerRequest());
            response.addItem(new Item().fetchFrom(resultSet));
            if (response.isComplete()||resultSet.isAfterLast()) {
                getSender().tell(response,getSelf());
            }
        }
    }
    catch (Exception e){
        throw new RuntimeException(e);
    }
}
}
