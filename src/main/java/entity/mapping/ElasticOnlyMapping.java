package entity.mapping;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 11/29/13
 * Time: 11:27
 */
public class ElasticOnlyMapping {
    private final String elasticName;

    public ElasticOnlyMapping(final String elasticName) {
        this.elasticName = elasticName.intern();
    }

    public String getElasticName() {
        return elasticName;
    }
}
