package entity.mapping;

import com.mysql.jdbc.StringUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/15/13
 * Time: 15:53
 */
public class FullMapping {
    private final String sqlColumnName;
    private final String sqlAlternativeColumn;
    private final String sqlChooseAlternativeColumnValue;
    private final String sqlColumnLabel;
    private final ElasticOnlyMapping elasticMapping;

    public FullMapping(String sqlColumnName, String sqlColumnLabel, String elasticMappingName) {
        this.sqlColumnName = sqlColumnName;
        this.sqlColumnLabel = sqlColumnLabel;
        this.elasticMapping = new ElasticOnlyMapping(elasticMappingName);
        this.sqlAlternativeColumn = "";
        this.sqlChooseAlternativeColumnValue = "";
    }

    public FullMapping(String sqlColumnName, String sqlColumnLabel, String elasticMappingName, String sqlAlternativeColumn, Object sqlChooseAlternativeColumnValue) {
        this.sqlColumnName = sqlColumnName;
        this.sqlColumnLabel = sqlColumnLabel;
        this.elasticMapping = new ElasticOnlyMapping(elasticMappingName);
        this.sqlAlternativeColumn = sqlAlternativeColumn;
        this.sqlChooseAlternativeColumnValue = sqlChooseAlternativeColumnValue.toString();
    }

    public static String selectStatementFromColumns(List<FullMapping> columns) {
        StringBuilder result = new StringBuilder("SELECT ");

        for (int i = 0; i < columns.size(); i++) {
            result.append(columns.get(i).getSelectSql());
            result.append(i < columns.size() - 1 ? ',' : ' ');
        }

        return result.toString();
    }

    private String getSelectSql() {
        String selectSql;
        if (StringUtils.isNullOrEmpty(sqlAlternativeColumn.trim())) {
            selectSql = String.format("%s AS '%s'".intern(), sqlColumnName, sqlColumnLabel);
        } else {
            selectSql = String.format("CASE WHEN %s = %s THEN %s ELSE %s END AS '%s'".intern(), sqlColumnName, sqlChooseAlternativeColumnValue,
                    sqlAlternativeColumn, sqlColumnName, sqlColumnLabel);
        }
        return selectSql;
    }

    public String getName() {
        return sqlColumnLabel.intern();
    }

    public String getElasticName() {
        return elasticMapping.getElasticName().intern();
    }
}
