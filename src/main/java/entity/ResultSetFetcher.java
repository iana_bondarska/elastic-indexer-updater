package entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/21/13
 * Time: 12:44 AM
 */
public interface ResultSetFetcher<T> {
    T fetchFrom(ResultSet resultSet) throws SQLException;
}
