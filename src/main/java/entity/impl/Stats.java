package entity.impl;


import entity.BaseMapped;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Stats extends BaseMapped<Object> {
    private static final int IGNORE_VALUE = -1;
    private final static FullMapping onlineTimestampColumn = new FullMapping("deposit_items.online_timestamp", "item.stats.online_timestamp", "online_timestamp", "deposit_items.upload_timestamp", 0);
    private final static FullMapping viewsColumn = new FullMapping("deposit_item_stats.views", "item.stats.views", "views");
    private final static FullMapping moneyColumn = new FullMapping("deposit_item_stats.money", "item.stats.money", "money");
    private final static FullMapping downloadsColumn = new FullMapping("deposit_item_stats.downloads", "item.stats.downloads", "downloads");
    private final static List<FullMapping> indexMappings = Lists.newArrayList(onlineTimestampColumn, viewsColumn, moneyColumn, downloadsColumn);

    public Stats() {
        super(getIndexMappings().size());
    }

    public long getOnlineTimestamp() {
        return (long) get(onlineTimestampColumn.getElasticName());
    }

    public long getViews() {
        return (long) get(viewsColumn.getElasticName());
    }

    public float getMoney() {
        return (float) get(moneyColumn.getElasticName());
    }

    public long getDownloads() {
        return (long) get(downloadsColumn.getElasticName());
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    @Override
    public Stats fetchFrom(final ResultSet resultSet) throws SQLException {
        this.put(onlineTimestampColumn.getElasticName(), resultSet.getLong(onlineTimestampColumn.getName()));
        this.put(viewsColumn.getElasticName(), resultSet.getLong(viewsColumn.getName()));
        this.put(moneyColumn.getElasticName(), resultSet.getFloat(moneyColumn.getName()));
        this.put(downloadsColumn.getElasticName(), resultSet.getLong(downloadsColumn.getName()));

        return this;
    }

    /**
     * method for creating new stats object by copying existing and replacing some values with method input
     * in case passed parameter equals -1, it is ignored and value is taken from existing stats object
     *
     * @param onlineTimeStamp onlineTimestamp value that should replace existing
     * @param views           views value that should replace existing
     * @param money           money value that should replace existing
     * @param downloads       downloads param value that should replace existing
     * @return new stats object that has same value as this object except replaced values
     */
    public Stats copyStats(long onlineTimeStamp, long views, float money, long downloads) {
        Stats stats = new Stats();
        stats.putAll(this);
        if (onlineTimeStamp != IGNORE_VALUE) {
            stats.put(onlineTimestampColumn.getElasticName(), onlineTimeStamp);
        }
        if (views != IGNORE_VALUE) {
            stats.put(viewsColumn.getElasticName(), views);
        }
        if (money != IGNORE_VALUE) {
            stats.put(moneyColumn.getElasticName(), money);
        }
        if (downloads != IGNORE_VALUE) {
            stats.put(downloadsColumn.getElasticName(), downloads);
        }
        return stats;
    }
}
