package entity.impl;


import entity.BaseMapped;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Author extends BaseMapped<Object> {
    private final static String rankCaseStr =
            "(case seller_internal_info.exam_rate" +
            "        when 7 then" +
            "            7 + 2 * (case seller_internal_info.color" +
            "                when 'a39' then 1" +
            "                when 'ff828e' then 2" +
            "                when 'ff829e' then 2" +
            "                else 0" +
            "            end)" +
            "        when 8 then" +
            "            9 + 3 * (case seller_internal_info.color" +
            "                when 'a39' then 1" +
            "                when 'ff828e' then 2" +
            "                when 'ff829e' then 2" +
            "                else 0" +
            "            end)" +
            "        when 9 then" +
            "            15 + 5 * (case seller_internal_info.color" +
            "                when 'a39' then 1" +
            "                when 'ff828e' then 2" +
            "                when 'ff829e' then 2" +
            "                else 0" +
            "            end)" +
            "        when 10 then" +
            "            20 + 5 * (case seller_internal_info.color" +
            "                when 'a39' then 1" +
            "                when 'ff828e' then 2" +
            "                when 'ff829e' then 2" +
            "                else 0" +
            "            end)" +
            "        else seller_internal_info.exam_rate " +
            "end)";
    private final static FullMapping idColumn = new FullMapping("deposit_items.user_id", "item.author.id", "id");
    private final static FullMapping usernameColumn = new FullMapping("aligator_engine_deposit.manager_user.username","item.author.username", "username");
    private final static FullMapping rankColumn = new FullMapping(rankCaseStr, "item.author.rank", "rank");
    private final static FullMapping userBonusColumn = new FullMapping("api_promotions.persent", "item.author.user_bonus", "user_bonus");
    private final static List<FullMapping> indexMappings = Lists.newArrayList(usernameColumn, rankColumn, idColumn, userBonusColumn);

    public Author() {
        super(getIndexMappings().size());
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    public long getUserBonus(){
       return (long) get(userBonusColumn.getElasticName());
    }

    public int getRank(){
        return (int) get(rankColumn.getElasticName());
    }

    @Override
    public Author fetchFrom(final ResultSet resultSet) throws SQLException {
        this.put(idColumn.getElasticName(), resultSet.getLong(idColumn.getName()));
        this.put(usernameColumn.getElasticName(), resultSet.getString(usernameColumn.getName()).intern());
        this.put(rankColumn.getElasticName(), resultSet.getInt(rankColumn.getName()));
        this.put(userBonusColumn.getElasticName(), resultSet.getLong(userBonusColumn.getName()));

        return this;
    }
}