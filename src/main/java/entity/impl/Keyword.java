package entity.impl;


import com.google.common.base.Strings;
import entity.BaseMapped;
import entity.mapping.ElasticOnlyMapping;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Keyword extends BaseMapped<Object> {
    private final static FullMapping keywordColumn = new FullMapping("keyword.keyword_name", "keyword", "keyword");
    private final static FullMapping weightColumn = new FullMapping("deposit_item_keyword.keyword_weight", "weight", "weight");
    private final static ElasticOnlyMapping bmKwsColumn = new ElasticOnlyMapping("bm_kws_weight");
    private final static List<FullMapping> indexMappings = Lists.newArrayList(keywordColumn, weightColumn);

    public Keyword() {
        super(getIndexMappings().size() + 1);
    }

    public Keyword(String keyword, long weight) {
        this();
        setKeyword(keyword);
        setWeight(weight);
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    private String keyword;
    public String getKeyword() {
        return keyword == null ? keyword = (String) this.get(keywordColumn.getElasticName()) : keyword;
    }

    public void setKeyword(String keyword) {
        this.put(keywordColumn.getElasticName(), keyword);
    }

    private Long weight;
    public long getWeight() {
        return weight == null ? weight = (long) this.get(weightColumn.getElasticName()) : weight;
    }

    public void setWeight(long weight) {
        this.put(weightColumn.getElasticName(), this.weight = weight);
    }

    public float getBmKwsWeight() {
        if(!containsKey(bmKwsColumn.getElasticName()))
            throw new RuntimeException("Keyword BmKws value accessed before being set.");

        return (float) get(bmKwsColumn.getElasticName());
    }

    public void setBmKwsWeight(float weight) {
        this.put(bmKwsColumn.getElasticName(), weight);
    }

    @Override
    public Keyword fetchFrom(final ResultSet resultSet) throws SQLException {
        String keyword = resultSet.getString(keywordColumn.getName());
        if (Strings.isNullOrEmpty(keyword))
            return null;

        this.put(keywordColumn.getElasticName(), keyword.intern());
        this.put(weightColumn.getElasticName(), resultSet.getLong(weightColumn.getName()));

        return this;
    }
}
