package entity.impl;


import entity.BaseMapped;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 11/28/13
 * Time: 13:52
 */
public class Series extends BaseMapped<Object> {
    private final static FullMapping isSeriesRootColumn = new FullMapping("deposit_item_serie.master", "item.series.is_series_root", "is_series_root");
    private final static FullMapping seriesIdColumn = new FullMapping("deposit_item_serie.serie_id", "item.series.series_id", "series_id");
    private final static List<FullMapping> indexMappings = Lists.newArrayList(isSeriesRootColumn, seriesIdColumn);

    public Series() {
        super(getIndexMappings().size());
    }

    public boolean isSeriesRoot() {
        return (boolean) get(isSeriesRootColumn.getElasticName());
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    @Override
    public Series fetchFrom(final ResultSet resultSet) throws SQLException {
        int seriesId = resultSet.getInt(seriesIdColumn.getName());
        this.put(seriesIdColumn.getElasticName(), seriesId);
        this.put(isSeriesRootColumn.getElasticName(), seriesId == 0 || resultSet.getBoolean(isSeriesRootColumn.getName()));

        return this;
    }
}
