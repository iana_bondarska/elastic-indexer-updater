package entity.impl;

import entity.BaseMapped;
import entity.mapping.ElasticOnlyMapping;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;
import utils.TypesCastHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MediaInfo extends BaseMapped<Object> {
    private final static FullMapping widthColumn = new FullMapping("deposit_items.width", "item.media_info.width",
                                                                   "width");
    private final static FullMapping heightColumn = new FullMapping("deposit_items.height", "item.media_info.height",
                                                                    "height");
    private final static FullMapping mpColumn = new FullMapping("deposit_items.mp", "item.media_info.mp", "mp");
    private final static FullMapping redColumn = new FullMapping("deposit_items.red", "item.media_info.red", "red");
    private final static FullMapping greenColumn = new FullMapping("deposit_items.green", "item.media_info.green",
                                                                   "green");
    private final static FullMapping blueColumn = new FullMapping("deposit_items.blue", "item.media_info.blue", "blue");
    private final static FullMapping colorIdCoumn = new FullMapping(
            "concat_ws('|', if(perc1 is not null and perc1 > 5, ifnull(color1,0), null)," +
                    "if(perc2 is not null and perc2 > 5, ifnull(color2,0), null)," +
                    "if(perc3 is not null and perc3 > 5, ifnull(color3,0), null))", "item.media_info.color_id",
            "color_id");
    private final static ElasticOnlyMapping mpStrColumn = new ElasticOnlyMapping("mp_str");
    private final static ElasticOnlyMapping orientationColumn = new ElasticOnlyMapping("orientation");
    private final static List<FullMapping> indexMappings = Lists
            .newArrayList(widthColumn, heightColumn, mpColumn, redColumn, greenColumn, blueColumn, colorIdCoumn);

    public MediaInfo() {
        super(getIndexMappings().size() + 2);
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    @Override
    public MediaInfo fetchFrom(final ResultSet resultSet) throws SQLException {
        this.put(redColumn.getElasticName(), resultSet.getInt(redColumn.getName()));
        this.put(greenColumn.getElasticName(), resultSet.getInt(greenColumn.getName()));
        this.put(blueColumn.getElasticName(), resultSet.getInt(blueColumn.getName()));
        this.put(colorIdCoumn.getElasticName(),
                 TypesCastHelper.parseArrayString(resultSet.getString(colorIdCoumn.getName())));

        float mpRaw = resultSet.getFloat(mpColumn.getName());
        this.put(mpColumn.getElasticName(), mpRaw);
        this.put(mpStrColumn.getElasticName(), Mp.valueOf(mpRaw));

        int width = resultSet.getInt(widthColumn.getName());
        this.put(widthColumn.getElasticName(), width);
        int height = resultSet.getInt(heightColumn.getName());
        this.put(heightColumn.getElasticName(), height);
        this.put(orientationColumn.getElasticName(), Orientation.valueOf(width, height));

        return this;
    }

    private enum Mp {
        XS, S, M, L, XL, XXL;

        public static Mp valueOf(float mpRaw) {
            if (mpRaw < 0.5)
                return XS;
            else if (mpRaw < 2)
                return S;
            else if (mpRaw < 8)
                return M;
            else if (mpRaw < 15)
                return L;
            else if (mpRaw < 24)
                return XL;
            else
                return XXL;
        }
    }

    private enum Orientation {
        horizontal(1.2f), vertical(0.79f), square(null);

        private final Float border;

        Orientation(final Float border) {
            this.border = border;
        }

        public static Orientation valueOf(int width, int height) {
            float propotion = (float) width / height;
            if (propotion > horizontal.border)
                return horizontal;
            else if (propotion < vertical.border)
                return vertical;
            else
                return square;
        }
    }
}