package entity.impl;


import entity.BaseMapped;
import entity.mapping.ElasticOnlyMapping;
import org.elasticsearch.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/16/13
 * Time: 1:52 PM
 */
public class Formulas extends BaseMapped<Object> {
    private final static ElasticOnlyMapping bmKwsColumn = new ElasticOnlyMapping("bm_kws");
    private final static ElasticOnlyMapping mpMaColumn = new ElasticOnlyMapping("mp_ma");
    private final static ElasticOnlyMapping bmMaColumn = new ElasticOnlyMapping("bm_ma");
    private final static List<ElasticOnlyMapping> indexMappings = Lists
            .newArrayList(bmKwsColumn, mpMaColumn, bmMaColumn);

    public Formulas() {
        super(indexMappings.size());
    }

    @Override
    public BaseMapped<Object> fetchFrom(final ResultSet resultSet) throws SQLException {
        return this;
    }

    public void setBmKws(float value){
        put(bmKwsColumn.getElasticName(), value);
    }

    public void setMpMa(float value){
        put(mpMaColumn.getElasticName(), value);
    }

    public void setBmMa(float value){
        put(bmMaColumn.getElasticName(), value);
    }
}
