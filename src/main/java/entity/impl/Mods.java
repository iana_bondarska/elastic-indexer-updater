package entity.impl;


import entity.BaseMapped;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;
import utils.TypesCastHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Mods extends BaseMapped<Object> {
    private final static FullMapping safeColumn = new FullMapping("deposit_items.is_nudity", "item.mods.is_unsafe", "is_unsafe");
    private final static FullMapping editorialColumn = new FullMapping("deposit_items.is_editorial", "item.mods.is_editorial", "is_editorial");
    private final static FullMapping mobileColumn = new FullMapping("deposit_items.is_mobile", "item.mods.is_mobile", "is_mobile");
    private final static List<FullMapping> indexMappings = Lists.newArrayList(safeColumn, editorialColumn, mobileColumn);

    public Mods() {
        super(getIndexMappings().size());
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    public boolean isUnsafe() {
        return (boolean) get(safeColumn.getElasticName());
    }

    public boolean isEditorial() {
        return (boolean) get(editorialColumn.getElasticName());
    }

    @Override
    public Mods fetchFrom(final ResultSet resultSet) throws SQLException {
        this.put(safeColumn.getElasticName(),
                 TypesCastHelper.getBooleanFromYesNo(resultSet.getString(safeColumn.getName()).intern()));
        this.put(editorialColumn.getElasticName(),
                 TypesCastHelper.getBooleanFromYesNo(resultSet.getString(editorialColumn.getName()).intern()));
        this.put(mobileColumn.getElasticName(),
                 TypesCastHelper.getBooleanFromYesNo(resultSet.getString(mobileColumn.getName()).intern()));

        return this;
    }
}