package entity.impl;


import entity.BaseMapped;
import entity.mapping.ElasticOnlyMapping;
import entity.mapping.FullMapping;
import org.elasticsearch.common.collect.Lists;
import utils.TypesCastHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Item extends BaseMapped<Object> {
    private final static FullMapping idColumn = new FullMapping("deposit_items.deposit_item_id", "item.id", "id");
    private final static FullMapping titleColumn = new FullMapping("deposit_items.title", "item.title", "title");
    private final static FullMapping descriptionColumn = new FullMapping("deposit_items.description", "item.description", "description");
    private final static FullMapping typeColumn = new FullMapping("deposit_items.type", "item.media_info.type", "type");
    private final static FullMapping uploadHostColumn = new FullMapping("deposit_items.upload_host", "item.upload_host", "upload_host");
    private final static FullMapping categoriesColumn = new FullMapping(
            "(select group_concat(deposit_item_category.category_id SEPARATOR '|') from deposit_item_category where " +
                    "deposit_item_category.deposit_item_id = deposit_items.deposit_item_id)",
            "categories", "categories");
    private final static FullMapping modelReleasesColumn = new FullMapping(
            "(select group_concat(deposit_item_model_release.model_release_id SEPARATOR '|') from deposit_item_model_release where "
                    + "deposit_item_model_release.deposit_item_id = deposit_items.deposit_item_id)",
            "model_releases", "model_releases");
    private final static List<FullMapping> indexMappings;
    private final static ElasticOnlyMapping formulasColumn = new ElasticOnlyMapping("formulas");
    private final static ElasticOnlyMapping modsColumn = new ElasticOnlyMapping("mods");
    private final static ElasticOnlyMapping statsColumn = new ElasticOnlyMapping("stats");
    private final static ElasticOnlyMapping mediaInfoColumn = new ElasticOnlyMapping("media_info");
    private final static ElasticOnlyMapping authorColumn = new ElasticOnlyMapping("author");
    private final static ElasticOnlyMapping keywordsColumn = new ElasticOnlyMapping("keywords");
    private final static ElasticOnlyMapping seriesColumn = new ElasticOnlyMapping("series");
    private final static ElasticOnlyMapping allKwStemmedColumn = new ElasticOnlyMapping("all_kw_stemmed");
    private final static ElasticOnlyMapping boostColumn = new ElasticOnlyMapping("boost");

    static {
        indexMappings = Lists.newArrayList(idColumn, titleColumn, descriptionColumn, typeColumn, uploadHostColumn,
                                           categoriesColumn, modelReleasesColumn);
        indexMappings.addAll(Mods.getIndexMappings());
        indexMappings.addAll(Stats.getIndexMappings());
        indexMappings.addAll(MediaInfo.getIndexMappings());
        indexMappings.addAll(Author.getIndexMappings());
        indexMappings.addAll(Series.getIndexMappings());
    }

    public Item() {
        super(getIndexMappings().size());
    }

    public static List<FullMapping> getIndexMappings() {
        return indexMappings;
    }

    public static List<Integer> getIdsFromItemsList(List<Item> items) {
        List<Integer> ids = new ArrayList<>(items.size());
        for (Item item : items)
            ids.add(item.getId());
        return ids;
    }

    public int getId() {
        return (int) get(idColumn.getElasticName());
    }

    public Series getSeries() {
        return (Series) get(seriesColumn.getElasticName());
    }

    public Formulas getFormulas() {
        return (Formulas) get(formulasColumn.getElasticName());
    }

    public String[] getModelReleases() {
        return (String[]) get(modelReleasesColumn.getElasticName());
    }

    public Author getAuthor() {
        return (Author) get(authorColumn.getElasticName());
    }

    public Stats getStats() {
        return (Stats) get(statsColumn.getElasticName());
    }

    public Mods getMods() {
        return (Mods) get(modsColumn.getElasticName());
    }

    private String title;
    public String getTitle() {
        return title == null ? title = (String) get(titleColumn.getElasticName()) : title;
    }

    public void setTitle(String title) {
        put(titleColumn.getElasticName(), this.title = title);
    }

    private String description;
    public String getDescription() {
        return description == null ? description = (String) get(descriptionColumn.getElasticName()) : description;
    }

    public void setDescription(String description) {
        put(descriptionColumn.getElasticName(), this.description = description);
    }

    public void setCategories(String[] categories) {
        this.put(categoriesColumn.getElasticName(), categories);
    }

    public void setModelRelease(String[] modelReleases) {
        this.put(modelReleasesColumn.getElasticName(), modelReleases);
    }

    public void setAllKwStemmed(String rawAllKwStemmed) {
        this.put(allKwStemmedColumn.getElasticName(), rawAllKwStemmed);
    }

    private List<Keyword> keywords;
    @SuppressWarnings("unchecked")
    public List<Keyword> getKeywords() {
        return keywords == null ? keywords = (List<Keyword>) this.get(keywordsColumn.getElasticName()) : keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.put(keywordsColumn.getElasticName(), this.keywords = keywords);
    }

    public void incrementBoost() {
        this.put(boostColumn.getElasticName(), 1 + (containsKey(boostColumn.getElasticName()) ?
                                                    (int)get(boostColumn.getElasticName()) : 0));
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        return getId() == ((Item) o).getId();
    }

    @Override
    public Item fetchFrom(final ResultSet resultSet) throws SQLException {
        this.put(idColumn.getElasticName(), resultSet.getInt(idColumn.getName()));
        this.put(titleColumn.getElasticName(), resultSet.getString(titleColumn.getName()).intern());
        this.put(descriptionColumn.getElasticName(), resultSet.getString(descriptionColumn.getName()).intern());
        this.put(typeColumn.getElasticName(), resultSet.getString(typeColumn.getName()).intern());
        this.put(uploadHostColumn.getElasticName(), resultSet.getString(uploadHostColumn.getName()).intern());
        this.put(modsColumn.getElasticName(), new Mods().fetchFrom(resultSet));
        this.put(statsColumn.getElasticName(), new Stats().fetchFrom(resultSet));
        this.put(mediaInfoColumn.getElasticName(), new MediaInfo().fetchFrom(resultSet));
        this.put(authorColumn.getElasticName(), new Author().fetchFrom(resultSet));
        this.put(seriesColumn.getElasticName(), new Series().fetchFrom(resultSet));
        this.put(formulasColumn.getElasticName(), new Formulas().fetchFrom(resultSet));
        this.put(categoriesColumn.getElasticName(),
                 TypesCastHelper.parseArrayString(resultSet.getString(categoriesColumn.getName())));
        this.put(modelReleasesColumn.getElasticName(),
                 TypesCastHelper.parseArrayString(resultSet.getString(modelReleasesColumn.getName())));

        return this;
    }

    public Item cleanForIndex() {
        //this.put(keywordsColumn.getElasticName(), getKeywords().values());
        return this;
    }
    public void updateStats(Stats incomingStats){
        this.put(statsColumn.getElasticName(),incomingStats);
    }
}