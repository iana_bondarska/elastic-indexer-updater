package entity;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/16/13
 * Time: 15:36
 */
public abstract class BaseMapped<T> extends HashMap<String, T> implements ResultSetFetcher<BaseMapped<T>> {
    public BaseMapped(final int initialCapacity) {
        super(initialCapacity < 3 ? 3 : (initialCapacity + initialCapacity / 3));
    }
}
