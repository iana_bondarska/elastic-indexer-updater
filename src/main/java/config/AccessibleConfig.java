package config;

/**
* Created with IntelliJ IDEA.
* User: Michael Cheremuhin
* Date: 12/17/13
* Time: 5:13 PM
*/
public interface AccessibleConfig {
    ElasticConfig getElasticSearch();

    JdbcConfig getJdbc();

    ThreadPoolConfig getThreadPool();

    AnalysisConfig getAnalysisConfig();
}
