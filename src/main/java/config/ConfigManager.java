package config;


import com.google.common.collect.Lists;
import com.google.gson.Gson;
import elastic.entity.ElasticIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.FilesHelper;

import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/17/13
 * Time: 5:10 PM
 */
public enum ConfigManager implements AccessibleConfig {
    INSTANCE;

    private final GlobalConfig config;
    private final String configFileName = "appconfig.json";
    private final String EMPTY_STRING = "";

    private ConfigManager() {
        Logger logger = LoggerFactory.getLogger(getClass());
        try {
            File configFile = new File(configFileName);
            String configJson = configFile.exists() ? FilesHelper.getStringFromFile(configFile)
                                                    : FilesHelper.getStringFromResource(configFileName);
            config = new Gson().fromJson(configJson, GlobalConfig.class);
            filterOutEmptyAliases(config.getElasticSearch().getElasticIndicies());
        } catch (Exception e) {
            logger.error("Could not parse config", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public ElasticConfig getElasticSearch() {
        return config.getElasticSearch();
    }

    @Override
    public JdbcConfig getJdbc() {
        return config.getJdbc();
    }

    @Override
    public ThreadPoolConfig getThreadPool() {
        return config.getThreadPool();
    }

    @Override
    public AnalysisConfig getAnalysisConfig() {
        return config.getAnalysisConfig();
    }

    private void filterOutEmptyAliases(List<ElasticIndex> elasticIndexes) {
        if (elasticIndexes != null && !elasticIndexes.isEmpty()) {
            for (ElasticIndex nextIndex : elasticIndexes) {
                if (nextIndex.getElAlias() != null) {
                    nextIndex.getElAlias().removeAll(Lists.newArrayList(EMPTY_STRING));
                }
            }
        }
    }

}
