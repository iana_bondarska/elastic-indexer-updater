package config;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/17/13
 * Time: 4:59 PM
 */
public class ThreadPoolConfig {
    private int maxThreads;
    private int maxWorkerThreads;

    public int getMaxThreads() {
        return maxThreads;
    }

    public int getMaxWorkerThreads() {
        return maxWorkerThreads;
    }
}
