package config;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 10/8/13
 * Time: 15:05
 */

public class GlobalConfig implements AccessibleConfig {
    private ElasticConfig elasticSearch;
    private JdbcConfig jdbc;
    private ThreadPoolConfig threadPool;
    private AnalysisConfig analysisConfig;

    @Override
    public ElasticConfig getElasticSearch() {
        return elasticSearch;
    }

    @Override
    public JdbcConfig getJdbc() {
        return jdbc;
    }

    @Override
    public ThreadPoolConfig getThreadPool() {
        return threadPool;
    }

    @Override
    public AnalysisConfig getAnalysisConfig() {
        return analysisConfig;
    }
}
