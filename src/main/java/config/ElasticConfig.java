package config;



import elastic.entity.ElasticIndex;
import elastic.entity.ElasticNode;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/17/13
 * Time: 5:03 PM
 */
public class ElasticConfig {
    private String clusterName;
    private int itemsPerBulk;
    private int updateFrequency;
    private List<ElasticNode> elasticNodes;
    private List<ElasticIndex> elasticIndicies;

    public String getClusterName() {
        return clusterName;
    }

    public int getItemsPerBulk() {
        return itemsPerBulk;
    }

    public List<ElasticNode> getElasticNodes() {
        return elasticNodes;
    }

    public List<ElasticIndex> getElasticIndicies() {
        return elasticIndicies;
    }

    public int getUpdateFrequency() {
        return updateFrequency;
    }
}
