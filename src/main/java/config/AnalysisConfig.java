package config;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 1/2/14
 * Time: 12:45 PM
 */
public class AnalysisConfig {
    private int shingleMaxLength;
    private int twoWordShingleWeight;
    private int maxWordsShingleWeight;
    private float titleShinglesMultiplier;
    private float descriptionShinglesMultiplier;
    private long onlineTimeStampShiftRange;
    private Set<String> boostKeywords;

    public int getShingleMaxLength() {
        return shingleMaxLength;
    }

    public int getTwoWordShingleWeight() {
        return twoWordShingleWeight;
    }

    public int getMaxWordsShingleWeight() {
        return maxWordsShingleWeight;
    }

    public float getTitleShinglesMultiplier() {
        return titleShinglesMultiplier;
    }

    public float getDescriptionShinglesMultiplier() {
        return descriptionShinglesMultiplier;
    }

    public Set<String> getBoostKeywords() {
        return boostKeywords;
    }

    public long getOnlineTimeStampShiftRange() {
        return onlineTimeStampShiftRange;
    }
}
