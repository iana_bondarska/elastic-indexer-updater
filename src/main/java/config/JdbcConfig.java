package config;

/**
 * Created with IntelliJ IDEA.
 * User: Michael Cheremuhin
 * Date: 12/17/13
 * Time: 4:59 PM
 */
public class JdbcConfig {
    private String url;
    private String username;
    private String password;
    private int minPoolSize;
    private int maxPoolSize;
    private int acquireIncrement;
    private int maxStatements;
    private int maxStatementsPerConnection;
    private int limitItemsCount;
    private int limitOnlineTimestampAfter;
    private boolean rootsOnly;

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public int getAcquireIncrement() {
        return acquireIncrement;
    }

    public int getMaxStatements() {
        return maxStatements;
    }

    public int getMaxStatementsPerConnection() {
        return maxStatementsPerConnection;
    }

    public int getLimitItemsCount() {
        return limitItemsCount;
    }

    public int getLimitOnlineTimestampAfter() {
        return limitOnlineTimestampAfter;
    }

    public boolean isRootsOnly() {
        return rootsOnly;
    }
}
